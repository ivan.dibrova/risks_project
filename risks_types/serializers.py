from rest_framework import serializers

from risks_types.models import RiskType, RiskTypeField, RiskTypeFieldEnum, Risk, RiskField


class RiskTypeFieldEnumSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField(required=False)

    class Meta:
        model = RiskTypeFieldEnum
        fields = ('id', 'value', )


class RiskTypeFieldSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField(required=False)
    enum_values = RiskTypeFieldEnumSerializer(many=True)

    class Meta:
        model = RiskTypeField
        fields = ('id', 'field_name', 'field_type', 'enum_values')


class RiskTypeSerializer(serializers.ModelSerializer):
    risk_type_fields = RiskTypeFieldSerializer(many=True)

    class Meta:
        model = RiskType
        ordering = ('id',)
        fields = ('id', 'type_name', 'risk_type_fields')

    def create(self, validated_data):
        risk_type_fields = validated_data.pop('risk_type_fields')
        risk_type = RiskType.objects.create(**validated_data)
        for risk_type_field_data in risk_type_fields:
            field_name = risk_type_field_data.get('field_name', None)
            field_type = risk_type_field_data.get('field_type', None)
            enum_values = risk_type_field_data.get('enum_values', [])

            if field_name and field_type:
                risk_type_field = RiskTypeField.objects.create(risk_type=risk_type, field_name=field_name,
                                                               field_type=field_type)
                if enum_values:
                    for enum_data in enum_values:
                        enum_value = enum_data.get('value', None)
                        if enum_value:
                            RiskTypeFieldEnum.objects.create(value=enum_value, risk_type_field=risk_type_field)
        return risk_type

    def update(self, instance, validated_data):
        risk_type_fields = validated_data.pop('risk_type_fields')

        instance.type_name = validated_data.get('type_name')
        instance.save()

        risk_fields_ids = []

        for risk_type_field_data in risk_type_fields:
            risk_type_field_id = risk_type_field_data.get('id', None)
            field_name = risk_type_field_data.get('field_name', None)
            field_type = risk_type_field_data.get('field_type', None)
            enum_values = risk_type_field_data.get('enum_values', [])

            if risk_type_field_id:
                risk_type_field = RiskTypeField.objects.get(id=risk_type_field_id)
                if field_name and field_type:
                    risk_type_field.field_name = field_name
                    risk_type_field.field_type = field_type
                    risk_type_field.save()
                risk_fields_ids.append(risk_type_field.id)

                enum_values_ids = []
                for enum_data in enum_values:
                    enum_data_id = enum_data.get('id', None)
                    enum_data_value = enum_data.get('value', None)
                    if enum_data_id:
                        if enum_data_value:
                            enum_item = RiskTypeFieldEnum.objects.get(id=enum_data_id)
                            enum_item.value = enum_data_value
                            enum_item.save()
                        enum_values_ids.append(enum_data_id)
                    else:
                        if enum_data_value:
                            enum_item = RiskTypeFieldEnum.objects.create(value=enum_data_value,
                                                                         risk_type_field=risk_type_field)
                            enum_values_ids.append(enum_item.id)

                for enum_item in RiskTypeFieldEnum.objects.filter(risk_type_field=risk_type_field):
                    if enum_item.id not in enum_values_ids:
                        enum_item.delete()

            else:
                if field_name and field_type:
                    risk_type_field = RiskTypeField.objects.create(risk_type=instance, field_name=field_name,
                                                                   field_type=field_type)
                    risk_fields_ids.append(risk_type_field.id)

                    if enum_values:
                        for enum_data in enum_values:
                            enum_value = enum_data.get('value', None)
                            if enum_value:
                                RiskTypeFieldEnum.objects.create(value=enum_value, risk_type_field=risk_type_field)

        for risk_type_field in RiskTypeField.objects.filter(risk_type=instance):
            if risk_type_field.id not in risk_fields_ids:
                RiskTypeFieldEnum.objects.filter(risk_type_field=risk_type_field).delete()
                risk_type_field.delete()

        return instance


class RiskFieldSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField(required=False)

    class Meta:
        model = RiskField
        fields = ('id', 'risk_type_field', 'value')


class RiskSerializer(serializers.ModelSerializer):
    risk_fields = RiskFieldSerializer(many=True)

    class Meta:
        model = Risk
        fields = ('id', 'risk_name', 'risk_type', 'risk_fields')

    def create(self, validated_data):
        risk_fields = validated_data.pop('risk_fields')
        risk = Risk.objects.create(**validated_data)
        for risk_field_data in risk_fields:
            RiskField.objects.create(risk=risk, **risk_field_data)
        return risk

    def update(self, instance, validated_data):
        risk_fields = validated_data.pop('risk_fields')
        for risk_field_data in risk_fields:
            risk_field_id = risk_field_data.get('id', None)
            if risk_field_id:
                risk_field = RiskField.objects.get(id=risk_field_id)
                risk_field.value = risk_field_data.get('value', risk_field.value)
                risk_field.save()

        instance.risk_name = validated_data.get('risk_name')
        instance.save()

        return instance

