from django.views.generic import View
from django.shortcuts import render

from rest_framework import generics

from risks_types.models import RiskType, Risk
from risks_types.serializers import RiskTypeSerializer, RiskSerializer


class MainPageView(View):
    template_name = 'index.html'

    def get(self, request, *args, **kwargs):
        return render(request, self.template_name)


class RiskTypesPageView(View):
    template_name = 'risk_types.html'

    def get(self, request, *args, **kwargs):
        return render(request, self.template_name)


class RiskTypesAddPageView(View):
    template_name = 'risk_types_add.html'

    def get(self, request, *args, **kwargs):
        return render(request, self.template_name)


class RiskTypesEditPageView(View):
    template_name = 'risk_types_edit.html'

    def get(self, request, *args, **kwargs):
        return render(request, self.template_name)


class RiskTypesList(generics.ListCreateAPIView):
    queryset = RiskType.objects.all().order_by('id')
    serializer_class = RiskTypeSerializer


class RiskTypesDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = RiskType.objects.all()
    serializer_class = RiskTypeSerializer


class RisksList(generics.ListCreateAPIView):
    queryset = Risk.objects.all()
    serializer_class = RiskSerializer


class RisksDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Risk.objects.all()
    serializer_class = RiskSerializer

