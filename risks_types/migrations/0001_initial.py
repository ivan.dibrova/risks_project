# Generated by Django 2.1.2 on 2018-10-09 12:18

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Risk',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('risk_name', models.CharField(max_length=255)),
            ],
        ),
        migrations.CreateModel(
            name='RiskBooleanField',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('value', models.BooleanField(default=False)),
                ('risk', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='risk_boolean', to='risks_types.Risk')),
            ],
        ),
        migrations.CreateModel(
            name='RiskDateField',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('value', models.DateField(blank=True, default=None, null=True)),
                ('risk', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='risk_date', to='risks_types.Risk')),
            ],
        ),
        migrations.CreateModel(
            name='RiskEnumField',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('risk', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='risk_enum', to='risks_types.Risk')),
            ],
        ),
        migrations.CreateModel(
            name='RiskIntegerField',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('value', models.IntegerField(blank=True, default=0, null=True)),
                ('risk', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='risk_integer', to='risks_types.Risk')),
            ],
        ),
        migrations.CreateModel(
            name='RiskTextField',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('value', models.CharField(blank=True, default='', max_length=255, null=True)),
                ('risk', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='risk_text', to='risks_types.Risk')),
            ],
        ),
        migrations.CreateModel(
            name='RiskType',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('type_name', models.CharField(max_length=255)),
            ],
        ),
        migrations.CreateModel(
            name='RiskTypeField',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('field_name', models.CharField(max_length=255)),
                ('field_type', models.CharField(choices=[('INT', 'Integer'), ('TXT', 'Text'), ('DAT', 'Date'), ('ENM', 'Enum'), ('BLN', 'Boolean')], default='TXT', max_length=3)),
                ('risk_type', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='risk_type_field', to='risks_types.RiskType')),
            ],
        ),
        migrations.CreateModel(
            name='RiskTypeFieldEnum',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('value', models.CharField(max_length=64)),
                ('enum_text_field', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='risk_type_field_enum', to='risks_types.RiskTypeField')),
            ],
        ),
        migrations.AddField(
            model_name='risktextfield',
            name='risk_type_field',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='risk_text_field', to='risks_types.RiskTypeField'),
        ),
        migrations.AddField(
            model_name='riskintegerfield',
            name='risk_type_field',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='risk_integer_field', to='risks_types.RiskTypeField'),
        ),
        migrations.AddField(
            model_name='riskenumfield',
            name='selected_value',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='risk_enum_value', to='risks_types.RiskTypeFieldEnum'),
        ),
        migrations.AddField(
            model_name='riskdatefield',
            name='risk_type_field',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='risk_date_field', to='risks_types.RiskTypeField'),
        ),
        migrations.AddField(
            model_name='riskbooleanfield',
            name='risk_type_field',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='risk_boolean_field', to='risks_types.RiskTypeField'),
        ),
        migrations.AddField(
            model_name='risk',
            name='risk_type',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='risk_type', to='risks_types.RiskType'),
        ),
    ]
