from django.db import models


class RiskType(models.Model):
    type_name = models.CharField(max_length=255)


class RiskTypeField(models.Model):
    FIELD_TYPE_INTEGER = 'Integer'
    FIELD_TYPE_TEXT = 'Text'
    FIELD_TYPE_DATE = 'Date'
    FIELD_TYPE_ENUM = 'Enum'
    FIELD_TYPE_BOOLEAN = 'CheckBox'

    FIELD_TYPE_CHOICES = (
        (FIELD_TYPE_INTEGER, 'Integer'),
        (FIELD_TYPE_TEXT, 'Text'),
        (FIELD_TYPE_DATE, 'Date'),
        (FIELD_TYPE_ENUM, 'Enum'),
        (FIELD_TYPE_BOOLEAN, 'CheckBox'),
    )

    risk_type = models.ForeignKey(RiskType, on_delete=models.CASCADE, related_name='risk_type_fields')
    field_name = models.CharField(max_length=255)
    field_type = models.CharField(max_length=8, choices=FIELD_TYPE_CHOICES, default=FIELD_TYPE_TEXT)

    class Meta:
        ordering = ('id',)


class RiskTypeFieldEnum(models.Model):
    value = models.CharField(max_length=64)
    risk_type_field = models.ForeignKey(RiskTypeField, on_delete=models.CASCADE, related_name='enum_values')

    class Meta:
        ordering = ('id',)


class Risk(models.Model):
    risk_name = models.CharField(max_length=255)
    risk_type = models.ForeignKey(RiskType, on_delete=models.CASCADE, related_name='risk_type')


class RiskField(models.Model):
    risk = models.ForeignKey(Risk, on_delete=models.CASCADE, related_name='risk_fields')
    risk_type_field = models.ForeignKey(RiskTypeField, on_delete=models.CASCADE, related_name='risk_type_field')
    value = models.CharField(max_length=255, null=True, blank=True, default='')

    class Meta:
        ordering = ('id',)



