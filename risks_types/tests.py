import json

from django.test import TestCase, Client
from rest_framework import status

from risks_types.models import RiskType, RiskTypeField, RiskTypeFieldEnum
from risks_types.serializers import RiskTypeSerializer


class GetPutDeleteRiskTypeTest(TestCase):
    def setUp(self):
        self.client = Client()

        # Add to database first risk type with fields for test of getting risk type
        risk_type = RiskType.objects.create(type_name='House')

        RiskTypeField.objects.create(risk_type=risk_type, field_name='Address', field_type='Text')
        RiskTypeField.objects.create(risk_type=risk_type, field_name='Square', field_type='Integer')
        RiskTypeField.objects.create(risk_type=risk_type, field_name='New', field_type='CheckBox')
        RiskTypeField.objects.create(risk_type=risk_type, field_name='Date of Sale', field_type='Date')
        enum_field = RiskTypeField.objects.create(risk_type=risk_type, field_name='Year of construction',
                                                  field_type='Enum')
        RiskTypeFieldEnum.objects.create(risk_type_field=enum_field, value='2018')
        RiskTypeFieldEnum.objects.create(risk_type_field=enum_field, value='2017')
        RiskTypeFieldEnum.objects.create(risk_type_field=enum_field, value='2016')

        # Add to database second risk type with fields for test of getting risk type
        risk_type = RiskType.objects.create(type_name='Car')

        RiskTypeField.objects.create(risk_type=risk_type, field_name='Model', field_type='Text')
        RiskTypeField.objects.create(risk_type=risk_type, field_name='Year', field_type='Integer')
        RiskTypeField.objects.create(risk_type=risk_type, field_name='New', field_type='CheckBox')
        RiskTypeField.objects.create(risk_type=risk_type, field_name='Date of Sale', field_type='Date')
        enum_field = RiskTypeField.objects.create(risk_type=risk_type, field_name='Color',
                                                  field_type='Enum')
        RiskTypeFieldEnum.objects.create(risk_type_field=enum_field, value='Green')
        RiskTypeFieldEnum.objects.create(risk_type_field=enum_field, value='Yellow')
        RiskTypeFieldEnum.objects.create(risk_type_field=enum_field, value='Red')
        RiskTypeFieldEnum.objects.create(risk_type_field=enum_field, value='White')
        RiskTypeFieldEnum.objects.create(risk_type_field=enum_field, value='Black')

        # First risk type for test of updating first risk type
        self.first_risk_type = {
            'id': 1,
            'type_name': 'House',
            'risk_type_fields': [
                {
                    'id': 1,
                    'field_name': 'Address',
                    'field_type': 'Text',
                    'enum_values': []
                },
                {
                    'id': 2,
                    'field_name': 'Square',
                    'field_type': 'Integer',
                    'enum_values': []
                },
                {
                    'id': 3,
                    'field_name': 'New',
                    'field_type': 'CheckBox',
                    'enum_values': []
                },
                {
                    'id': 4,
                    'field_name': 'Date of Sale House',         # edit field name
                    'field_type': 'Date',
                    'enum_values': []
                },
                {
                    'id': 5,
                    'field_name': 'Year of Construction',
                    'field_type': 'Enum',
                    'enum_values': [
                        {
                            'id': 1,
                            'value': '2018'
                        },
                        {
                            'id': 2,
                            'value': '2017'
                        },
                        {
                            'id': 3,
                            'value': '2016'
                        },
                        {
                            'value': '2015'                     # new enum field
                        }
                    ]
                },
                {
                    'field_name': 'Number',                     # new risk type field
                    'field_type': 'Integer',
                    'enum_values': []
                }
            ]
        }

        # Second risk type for test of updating second risk type
        self.second_risk_type = {
            'id': 2,
            'type_name': 'Car',
            'risk_type_fields': [
                {
                    'id': 6,
                    'field_name': 'Model',
                    'field_type': 'Text',
                    'enum_values': []
                },
                {
                    'id': 7,
                    'field_name': 'Year',
                    'field_type': 'Enum',                       # edit field type to enum with adding new enum fields
                    'enum_values': [
                        {
                            'value': '2018'                     # new enum field
                        },
                        {
                            'value': '2017'                     # new enum field
                        },
                        {
                            'value': '2016'                     # new enum field
                        },
                        {
                            'value': '2015'                     # new enum field
                        }
                    ]
                },
                                                                # delete field "New"
                {
                    'id': 9,
                    'field_name': 'Date of Sale Car',           # edit field name
                    'field_type': 'Date',
                    'enum_values': []
                },
                {
                    'id': 10,
                    'field_name': 'Year of Construction',
                    'field_type': 'Enum',
                    'enum_values': [
                        {
                            'id': 4,
                            'value': '2018'
                        },
                        {
                            'id': 5,
                            'value': '2017'
                        },
                        {
                            'id': 6,
                            'value': '2016'
                        },
                        {
                            'value': '2015'                     # new enum field
                        }
                    ]
                },
                {
                    'field_name': 'Used',                       # new risk type field
                    'field_type': 'CheckBox',
                    'enum_values': []
                }
            ]
        }

    def test_get_all_risk_types(self):
        # get risk types from API
        response = self.client.get('/api/risk_types/')

        # get risk types from db
        risk_types = RiskType.objects.all().order_by('id')
        serializer = RiskTypeSerializer(risk_types, many=True)

        self.assertEqual(response.data, serializer.data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_get_single_risk_type(self):
        # get single risk types from API
        response_1 = self.client.get('/api/risk_types/1/')
        response_2 = self.client.get('/api/risk_types/2/')

        # get single risk types from db
        risk_type_1 = RiskType.objects.get(pk=1)
        risk_type_2 = RiskType.objects.get(pk=2)
        serializer_1 = RiskTypeSerializer(risk_type_1)
        serializer_2 = RiskTypeSerializer(risk_type_2)

        self.assertEqual(response_1.data, serializer_1.data)
        self.assertEqual(response_1.status_code, status.HTTP_200_OK)
        self.assertEqual(response_2.data, serializer_2.data)
        self.assertEqual(response_2.status_code, status.HTTP_200_OK)

    def test_update_risk_type(self):
        # put single risk types to API
        response_1 = self.client.put('/api/risk_types/1/', data=json.dumps(self.first_risk_type),
                                    content_type='application/json')
        response_2 = self.client.put('/api/risk_types/2/', data=json.dumps(self.second_risk_type),
                                    content_type='application/json')

        self.assertEqual(response_1.status_code, status.HTTP_200_OK)
        self.assertEqual(response_2.status_code, status.HTTP_200_OK)

    def test_delete_single_risk_type(self):
        # delete single risk types from API
        response_1 = self.client.delete('/api/risk_types/1/')
        response_2 = self.client.delete('/api/risk_types/2/')

        # Check existence of risk types in database
        risk_types_quantity = RiskType.objects.all().count()

        self.assertEqual(response_1.status_code, status.HTTP_204_NO_CONTENT)
        self.assertEqual(response_2.status_code, status.HTTP_204_NO_CONTENT)
        self.assertEqual(risk_types_quantity, 0)


class PostRiskTypeTest(TestCase):
    def setUp(self):
        self.client = Client()

        # Risk type for test of creating new risk type
        self.risk_type = {
            'type_name': 'House',
            'risk_type_fields': [
                {
                    'field_name': 'Address',
                    'field_type': 'Text',
                    'enum_values': []
                },
                {
                    'field_name': 'Square',
                    'field_type': 'Integer',
                    'enum_values': []
                },
                {
                    'field_name': 'New',
                    'field_type': 'CheckBox',
                    'enum_values': []
                },
                {
                    'field_name': 'Date of Sale',
                    'field_type': 'Date',
                    'enum_values': []
                },
                {
                    'field_name': 'Year of Construction',
                    'field_type': 'Enum',
                    'enum_values': [
                        {
                            'value': '2018'
                        },
                        {
                            'value': '2017'
                        },
                        {
                            'value': '2016'
                        }
                    ]
                }
            ]
        }

    def test_create_risk_type(self):
        # post risk type to API
        response = self.client.post('/api/risk_types/', data=json.dumps(self.risk_type),
                                    content_type='application/json')

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

