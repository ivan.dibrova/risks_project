from django.apps import AppConfig


class RisksTypesConfig(AppConfig):
    name = 'risks_types'
