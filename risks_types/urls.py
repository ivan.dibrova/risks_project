from django.urls import path

from risks_types.views import MainPageView, RiskTypesPageView, RiskTypesList, RiskTypesDetail, \
    RisksList, RisksDetail, RiskTypesAddPageView, RiskTypesEditPageView


urlpatterns = [
    path('', MainPageView.as_view(), name='main_page'),
    path('risk_types/', RiskTypesPageView.as_view(), name='risk_types'),
    path('risk_types/add/', RiskTypesAddPageView.as_view()),
    path('risk_types/edit/<int:pk>/', RiskTypesEditPageView.as_view()),

    path('api/risk_types/', RiskTypesList.as_view()),
    path('api/risk_types/<int:pk>/', RiskTypesDetail.as_view()),
    path('api/risks/', RisksList.as_view()),
    path('api/risks/<int:pk>/', RisksDetail.as_view()),
]