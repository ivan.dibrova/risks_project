new Vue({
    el: '#risk_types_edit',
    delimiters: ['${','}'],
    data: {
        riskTypeId: 0,
        riskTypeFields: [],
        newRiskTypeField: {'id': null, 'field_name': null, 'field_type': null, 'enum_values': [] },
        currentRiskTypeField: {'index': 0, 'id': null, 'field_name': null, 'field_type': null, 'enum_values': [] },
        typeName: '',
        currentRiskType: '',
        currentEnumValues: [],
        currentEditEnumValues: [],
        currentEnumValue: {'id': null, 'value': null},
        currentEditEnumValue: {'id': null, 'value': null}

    },

    mounted: function() {
        this.getRiskType();
    },

    methods: {

        getRiskType: function() {
            this.riskTypeId = window.location.pathname.split('/')[3];
            let id = this.riskTypeId;

            console.log(id);
            console.log(`/api/risk_types/${id}/`);

            this.$http.get(`/api/risk_types/${id}/`)
                .then((response) => {
                    this.currentRiskType = response.data;
                    this.typeName = this.currentRiskType['type_name'];
                    this.riskTypeFields = this.currentRiskType['risk_type_fields'];
                })
                .catch((err) => {
                    console.log(err);
                })
        },

        deleteRiskTypeField: function(index) {
            this.riskTypeFields.splice(index, 1)
        },

        editRiskTypeField: function(index) {
            this.currentEditEnumValues = [];
            this.currentEditEnumValue = {'id': null, 'value': null};

            this.currentRiskTypeField = {
                                         'index': index,
                                         'id': this.riskTypeFields[index]['id'],
                                         'field_name': this.riskTypeFields[index]['field_name'],
                                         'field_type': this.riskTypeFields[index]['field_type'],
                                         'enum_values': this.riskTypeFields[index]['enum_values']
                                        };
            this.currentEditEnumValues = this.riskTypeFields[index]['enum_values'];

            $("#editRiskTypeFieldModal").modal('show');
        },

        addNewRiskTypeField: function() {
            this.newRiskTypeField = {'id': null, 'field_name': null, 'field_type': null, 'enum_values': [] };
            this.currentEnumValues = [];
            this.currentEnumValue = {'id': null, 'value': null};

            $("#addRiskTypeFieldModal").modal('show');
        },

        saveAddNewRiskTypeField: function() {
            if (this.newRiskTypeField['field_name'] === null || this.newRiskTypeField['field_name'].length === 0) {
                $("#field_name_error").show().text('Field name can\'t be empty!')
            } else if (this.newRiskTypeField['field_type'] === null || this.newRiskTypeField['field_type'].length === 0)
            {
                $("#field_type_error").show().text('Select field type!')
            } else if (this.newRiskTypeField['field_type'] === 'Enum' && this.currentEnumValues.length === 0) {
                $("#field_type_error").show().text('Add enum values for enum type!')
            } else {
                let enum_values = [];

                if (this.newRiskTypeField['field_type'] === 'Enum') {
                    this.currentEnumValues.forEach( function(element) {
                        enum_values.push(element);
                    });
                }

                this.riskTypeFields.push({
                          'id': null,
                          'field_name': this.newRiskTypeField['field_name'],
                          'field_type': this.newRiskTypeField['field_type'],
                          'enum_values': enum_values
                         });
                $("#field_name_error").hide();
                $("#field_type_error").hide();

                newRiskTypeField = {'id': null, 'field_name': null, 'field_type': null, 'enum_values': [] };
                $("#enum_table tr").remove();

                $("#addRiskTypeFieldModal").modal('hide');
            }
        },

        addEnumValue: function() {
            if (this.currentEnumValue['value'] === null || this.currentEnumValue['value'].length === 0) {
                $("#enum_value_error").show().text('Enum value can\'t be empty!')
            } else {
                $("#field_name_error").hide();
                $("#enum_value_error").hide();
                $("#field_type_error").hide();
                this.currentEnumValues.push(this.currentEnumValue)
            }
        },

        addEditEnumValue: function() {
            if (this.currentEditEnumValue['value'] === null || this.currentEditEnumValue['value'].length === 0) {
                $("#edit_enum_value_error").show().text('Enum value can\'t be empty!')
            } else {
                $("#edit_field_name_error").hide();
                $("#edit_enum_value_error").hide();
                $("#edit_field_type_error").hide();
                this.currentEditEnumValues.push(this.currentEditEnumValue)
            }
        },

        deleteEnumValue: function(index) {
            this.currentEnumValues.splice(index, 1)
        },

        deleteEditEnumValue: function(index) {

            this.currentEditEnumValues.splice(index, 1)
        },

        saveEditedRiskTypeField: function() {
            if (this.currentRiskTypeField['field_name'] === null ||
                                                                this.currentRiskTypeField['field_name'].length === 0) {
                $("#edit_field_name_error").show().text('Field name can\'t be empty!')
            } else if (this.currentRiskTypeField['field_type'] === null ||
                                                                this.currentRiskTypeField['field_type'].length === 0) {
                $("#edit_field_type_error").show().text('Select field type!')
            } else if (this.currentRiskTypeField['field_type'] === 'Enum' && this.currentEditEnumValues.length === 0) {
                $("#edit_field_type_error").show().text('Add enum values for enum type!')
            } else {

                let edit_enum_values = [];

                if (this.currentRiskTypeField['field_type'] === 'Enum') {
                    this.currentEditEnumValues.forEach( function(element) {
                        edit_enum_values.push(element);
                    });
                }

                this.riskTypeFields[this.currentRiskTypeField['index']] = {
                                                                'id': this.currentRiskTypeField['id'],
                                                                'field_name': this.currentRiskTypeField['field_name'],
                                                                'field_type': this.currentRiskTypeField['field_type'],
                                                                'enum_values': edit_enum_values
                };
                $("#edit_field_name_error").hide();
                $("#edit_field_type_error").hide();

                $("#editRiskTypeFieldModal").modal('hide');
            }
        },

        saveUpdatedRiskType: function() {
            if (this.typeName === null || this.typeName.length === 0) {
                $("#risk_type_error").show().text('Risk type name can\'t be empty!')
            } else {
                let risk_types_fields = [];

                this.riskTypeFields.forEach( function(element) {
                    let enum_values_str = element['enum_values'];
                    let enum_values_dict = [];

                    enum_values_str.forEach( function (enum_value) {
                        if (enum_value['id'] !== null) {
                            enum_values_dict.push(enum_value);
                        } else {
                            enum_values_dict.push({'value': enum_value['value']});
                        }
                    });

                    if (element['id'] !== null) {
                        risk_types_fields.push({
                                                'id': element['id'],
                                                'field_name': element['field_name'],
                                                'field_type': element['field_type'],
                                                'enum_values': enum_values_dict
                        });
                    } else {
                        risk_types_fields.push({
                                                'field_name': element['field_name'],
                                                'field_type': element['field_type'],
                                                'enum_values': enum_values_dict
                        });
                    }

                });

                this.currentRiskType = {'id': this.riskTypeId,
                                        'type_name': this.typeName,
                                        'risk_type_fields': risk_types_fields};

                this.$http.put(`/api/risk_types/${this.currentRiskType.id}/`, this.currentRiskType)
                    .then((response) => {
                        window.location.href = '/risk_types';
                    })
                    .catch((err) => {
                        console.log(err);
                    })
            }

        },

    }
});