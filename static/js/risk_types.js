new Vue({
    el: '#risk_types_list',
    delimiters: ['${','}'],
    data: {
        riskTypes: [],
        counter: 0,
        currentRiskType: {},
        message: null,
        newRiskType: { 'type_name': null, 'risk_type_fields': null },
        search_term: '',
    },

    mounted: function() {
        this.getRiskTypes();
    },

    methods: {
        getRiskTypes: function() {
            let api_url = '/api/risk_types/';
            let search_term = this.search_term;

            if (search_term !== '' && search_term !== null) {
                this.$http.get(api_url)
                    .then((response) => {

                        let searchedRiskTypes = [];

                        response.data.forEach(function(element) {
                            if (element['type_name'].indexOf(search_term) !== -1) {
                                searchedRiskTypes.push(element);
                            }
                        });

                        this.riskTypes = searchedRiskTypes;
                    })
                    .catch((err) => {
                        console.log(err);
                    })
            } else {
                this.$http.get(api_url)
                    .then((response) => {
                        this.riskTypes = response.data;
                    })
                    .catch((err) => {
                        console.log(err);
                    })
            }
        },

        getRiskType: function(id) {
            this.loading = true;
            this.$http.get(`/api/risk_types/${id}/`)
                .then((response) => {
                    this.currentRiskType = response.data;
                })
                .catch((err) => {
                    console.log(err);
                })
        },

        deleteRiskType: function(id) {
            this.loading = true;
            this.$http.delete(`/api/risk_types/${id}/`)
                .then((response) => {
                    this.getRiskTypes();
                })
                .catch((err) => {
                    console.log(err);
                })
        }

    }
});