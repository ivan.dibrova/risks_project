from django.contrib import admin
from django.conf.urls import include
from django.urls import path


urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include(('risks_types.urls', 'risks_types'), namespace='risks_types')),
]
