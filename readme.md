Test Project for Risk Management

https://risksproject.herokuapp.com

UI
1. List of risk types
2. Add new risk type
3. Edit risk type
4. Delete risk type

REST API
1. List of risk types:
GET https://risksproject.herokuapp.com/api/risk_types/
2. One risk type:
GET https://risksproject.herokuapp.com/api/risk_types/{id}/
3. Add risk type:
POST https://risksproject.herokuapp.com/api/risk_types/
4. Edit (update) risk type:
PUT https://risksproject.herokuapp.com/api/risk_types/{id}/
5. Delete risk type:
DELETE https://risksproject.herokuapp.com/api/risk_types/{id}/

Install project (Ubuntu 16.04)
1. sudo apt-get update
2. sudo apt-get install python3.6
3. sudo apt-get install virtualenv
4. sudo apt-get install nodejs
5. sudo npm install -g bower
6. git clone git@gitlab.com:ivan.dibrova/risks_project.git
7. cd risks_project
8. python3.6 -m virtualenv venv
9. cd venv/bin
10. source activate
11. cd ../..
12. pip3 install --user --upgrade setuptools
13. pip3 install --user -r requirements.txt
14. bower install
15. python3.6 manage.py collectstatic --settings risks_project.test
16. python3.6 manage.py makemigrations --settings risks_project.test
17. python3.6 manage.py migrate --settings risks_project.test
18. python3.6 manage.py runserver --settings risks_project.test
19. http://127.0.0.1:8000/